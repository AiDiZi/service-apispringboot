package aidar.com.webservice.SchemaGenerator;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaExport.Action;
import org.hibernate.tool.schema.TargetType;

import aidar.com.webservice.Models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;


public class SchemaGenerator {


    public void GenerateSchema(){
        Map<String, String> settings = new HashMap<>();

        settings.put("dialect",
                "org.hibernate.dialect.PostgreSQL82Dialect");
        settings.put("hibernate.connection.url",
                "jdbc:postgresql://localhost:5432/Service-Api");

        settings.put("hibernate.connection.username", "postgres");
        settings.put("hibernate.connection.password", "Darik1234");
        settings.put("hibernate.show_sql", "true");
        settings.put("hibernate.format_sql", "true");

        ServiceRegistry serviceRegistry =
                new StandardServiceRegistryBuilder().applySettings(settings).build();

        MetadataSources metadata =
                new MetadataSources(serviceRegistry);
        metadata.addAnnotatedClass(Customer.class);
        metadata.addAnnotatedClass(Employee.class);
        metadata.addAnnotatedClass(Order.class);
        metadata.addAnnotatedClass(Person.class);
        metadata.addAnnotatedClass(Service.class);

        EnumSet<TargetType> enumSet = EnumSet.of(TargetType.DATABASE);
        SchemaExport schemaExport = new SchemaExport();
        schemaExport.execute(enumSet, Action.BOTH, metadata.buildMetadata());
    }

}
