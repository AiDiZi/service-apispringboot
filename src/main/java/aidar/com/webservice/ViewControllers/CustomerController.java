package aidar.com.webservice.ViewControllers;

import aidar.com.webservice.Services.CustomerServices.CustomerServices;
import aidar.com.webservice.Services.CustomerServices.ICustomerServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/Customer")
public class CustomerController {

    @Autowired
    @Qualifier("CustomerServiceDB")
    private ICustomerServices _customerServices;

    @GetMapping("/AllCustomers")
    public String AllCustomers(Model model) {
        var customers = _customerServices.GetAllCustomers();
        model.addAttribute("customers", customers);
        return "customers";
    }

    /*@GetMapping("/Customer/{part}")
    public String PartCustomers(@PathVariable int part, Model model){
        var cutomers = _customerServices.GetAllCustomers();
        return "customers";
    } */
}
