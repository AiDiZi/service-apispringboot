package aidar.com.webservice.Services.EmployeeServices;


import aidar.com.webservice.Models.Employee;

import java.util.List;
import java.util.Optional;

public interface IEmployeeServices {

    Optional<Employee> GetEmployeeById(int id);

    List<Employee> GetAllEmployee();

    void AddEmployee(Employee employee);

    void AddEmployees(List<Employee> employees);

    void UpdateEmployee(Employee employee);

    void UpdateEmployees(List<Employee> employees);

    void DeleteEmployee(Employee employee);

    void DeleteEmployees(List<Employee> employees);

}
