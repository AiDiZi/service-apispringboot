package aidar.com.webservice.Services.EmployeeServices;

import aidar.com.webservice.DAO.Employee.EmployeeDAO;
import aidar.com.webservice.Models.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Optional;

public class EmployeeService implements IEmployeeServices{

    @Autowired
    @Qualifier("EmployeeDAODB")
    private EmployeeDAO _employeeDAO;

    public EmployeeService(){}

    @Override
    public Optional<Employee> GetEmployeeById(int id){
        return _employeeDAO.GetById(id);
    }

    @Override
    public List<Employee> GetAllEmployee(){
        return _employeeDAO.GetAll();
    }

    @Override
    public void AddEmployee(Employee employee){
        _employeeDAO.Add(employee);
    }

    @Override
    public void AddEmployees(List<Employee> employees){
        _employeeDAO.AddRange(employees);
    }

    @Override
    public void UpdateEmployee(Employee employee){
        _employeeDAO.Update(employee);
    }

    @Override
    public void UpdateEmployees(List<Employee> employees){
        _employeeDAO.UpdateRange(employees);
    }

    @Override
    public void DeleteEmployee(Employee employee){
        _employeeDAO.Delete(employee);
    }

    @Override
    public void DeleteEmployees(List<Employee> employees){
        _employeeDAO.DeleteRange(employees);
    }

}
