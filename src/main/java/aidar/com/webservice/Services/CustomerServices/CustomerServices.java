package aidar.com.webservice.Services.CustomerServices;

import aidar.com.webservice.DAO.Customer.CustomerDAO;
import aidar.com.webservice.Models.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;
import java.util.Optional;

public class CustomerServices implements ICustomerServices{

    @Autowired
    @Qualifier("CustomerDAODB")
    private CustomerDAO _customerDAO;

    public CustomerServices(){}


    @Override
    public List<Customer> GetAllCustomers(){
        return _customerDAO.GetAll();
    }

    @Override
    public Optional<Customer> GetCustomerById(int id){
        return _customerDAO.GetById(id);
    }

    @Override
    public void AddCustomer(Customer customer){
        _customerDAO.Add(customer);
    }

    @Override
    public void AddCustomers(List<Customer> customers){
        _customerDAO.AddRange(customers);
    }

    @Override
    public void UpdateCustomer(Customer customer){
        _customerDAO.Update(customer);
    }

    @Override
    public void UpdateCustomers(List<Customer> customers){
        _customerDAO.UpdateRange((customers));
    }

    @Override
    public void DeleteCustomer(Customer customer){
        _customerDAO.Delete(customer);
    }

    @Override
    public void DeleteCustomers(List<Customer> customers){
        _customerDAO.DeleteRange(customers);
    }

}
