package aidar.com.webservice.Services.CustomerServices;



import aidar.com.webservice.Models.Customer;

import java.util.List;
import java.util.Optional;

public interface ICustomerServices {

    Optional<Customer> GetCustomerById(int id);

    List<Customer> GetAllCustomers();

    void AddCustomer(Customer customer);

    void AddCustomers(List<Customer> customers);

    void UpdateCustomer(Customer customer);

    void UpdateCustomers(List<Customer> customers);

    void DeleteCustomer(Customer customer);

    void DeleteCustomers(List<Customer> customers);

}
