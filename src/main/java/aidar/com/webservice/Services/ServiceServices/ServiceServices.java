package aidar.com.webservice.Services.ServiceServices;



import aidar.com.webservice.DAO.Service.ServiceDAO;
import aidar.com.webservice.Models.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;
import java.util.Optional;

public class ServiceServices implements IServiceServices{

    @Autowired
    @Qualifier("ServiceDAODB")
    private ServiceDAO _serviceDAO;

    public ServiceServices(){}

    @Override
    public Optional<Service> GetServiceById(int id){
        return _serviceDAO.GetById(id);
    }

    @Override
    public List<Service> GetAllService(){
        return _serviceDAO.GetAll();
    }

    @Override
    public void AddService(Service Service){
        _serviceDAO.Add(Service);
    }

    @Override
    public void AddServices(List<Service> Services){
        _serviceDAO.AddRange(Services);
    }

    @Override
    public void UpdateService(Service Service){
        _serviceDAO.Update(Service);
    }

    @Override
    public void UpdateServices(List<Service> Services){
        _serviceDAO.UpdateRange(Services);
    }

    @Override
    public void DeleteService(Service Service){
        _serviceDAO.Delete(Service);
    }

    @Override
    public void DeleteServices(List<Service> Services){
        _serviceDAO.DeleteRange(Services);
    }
}
