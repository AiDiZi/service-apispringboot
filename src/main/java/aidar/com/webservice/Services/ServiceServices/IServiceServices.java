package aidar.com.webservice.Services.ServiceServices;


import aidar.com.webservice.Models.Service;

import java.util.*;

public interface IServiceServices {

    Optional<Service> GetServiceById(int id);

    List<Service> GetAllService();

    void AddService(Service Service);

    void AddServices(List<Service> Services);

    void UpdateService(Service Service);

    void UpdateServices(List<Service> Services);

    void DeleteService(Service Service);

    void DeleteServices(List<Service> Services);
}
