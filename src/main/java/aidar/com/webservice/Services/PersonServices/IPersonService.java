package aidar.com.webservice.Services.PersonServices;

import aidar.com.webservice.Models.Person;

import java.util.*;

public interface IPersonService {
    
    Optional<Person> GetPersonById(int id);

    List<Person> GetAllPerson();

    void AddPerson(Person Person);

    void AddPersons(List<Person> Persons);

    void UpdatePerson(Person Person);

    void UpdatePersons(List<Person> Persons);

    void DeletePerson(Person Person);

    void DeletePersons(List<Person> Persons);
}
