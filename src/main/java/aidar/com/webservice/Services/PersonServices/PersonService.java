package aidar.com.webservice.Services.PersonServices;

import aidar.com.webservice.DAO.Person.PersonDAO;
import aidar.com.webservice.Models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Optional;

public class PersonService implements IPersonService {

    @Autowired
    @Qualifier("PersonDAODB")
    private PersonDAO _personDAO;

    public PersonService(){}


    @Override
    public Optional<Person> GetPersonById(int id){
        return _personDAO.GetById(id);
    }

    @Override
    public List<Person> GetAllPerson(){
        return _personDAO.GetAll();
    }

    @Override
    public void AddPerson(Person Person){
        _personDAO.Add(Person);
    }

    @Override
    public void AddPersons(List<Person> Persons){
        _personDAO.AddRange(Persons);
    }

    @Override
    public void UpdatePerson(Person Person){
        _personDAO.Update(Person);
    }

    @Override
    public void UpdatePersons(List<Person> Persons){
        _personDAO.UpdateRange(Persons);
    }

    @Override
    public void DeletePerson(Person Person){
        _personDAO.Delete(Person);
    }

    @Override
    public void DeletePersons(List<Person> Persons){
        _personDAO.DeleteRange(Persons);
    }
}
