package aidar.com.webservice.Services.OrderService;


import aidar.com.webservice.Models.Order;

import java.util.List;
import java.util.Optional;

public interface IOrderService {

    Optional<Order> GetOrderById(int id);

    List<Order> GetAllOrder();

    void AddOrder(Order Order);

    void AddOrders(List<Order> Orders);

    void UpdateOrder(Order Order);

    void UpdateOrders(List<Order> Orders);

    void DeleteOrder(Order Order);

    void DeleteOrders(List<Order> Orders);


}
