package aidar.com.webservice.Services.OrderService;

import aidar.com.webservice.DAO.Order.OrderDAO;
import aidar.com.webservice.Models.Order;
import aidar.com.webservice.Services.OrderService.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Optional;

public class OrderService implements IOrderService {

    @Autowired
    @Qualifier("OrderDAODB")
    private OrderDAO _orderDAO;

    public OrderService(){}


    @Override
    public Optional<Order> GetOrderById(int id){
        return  _orderDAO.GetById(id);
    }

    @Override
    public List<Order> GetAllOrder(){
        return _orderDAO.GetAll();
    }

    @Override
    public void AddOrder(Order Order){
        _orderDAO.Add(Order);
    }

    @Override
    public void AddOrders(List<Order> Orders){
        _orderDAO.AddRange(Orders);
    }

    @Override
    public void UpdateOrder(Order Order){
        _orderDAO.Update(Order);
    }

    @Override
    public void UpdateOrders(List<Order> Orders){
        _orderDAO.UpdateRange(Orders);
    }

    @Override
    public void DeleteOrder(Order Order){
        _orderDAO.Delete(Order);
    }

    @Override
    public void DeleteOrders(List<Order> Orders){
        _orderDAO.DeleteRange(Orders);
    }

}
