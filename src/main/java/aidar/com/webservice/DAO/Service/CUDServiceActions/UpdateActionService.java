package aidar.com.webservice.DAO.Service.CUDServiceActions;

import aidar.com.webservice.DAO.ModifyAction.ModifyAction;
import aidar.com.webservice.Models.Service;
import org.hibernate.Session;

public class UpdateActionService implements ModifyAction<Service> {

    @Override
    public void DoModifyAction(Service service, Session session){
        session.update(service);
    }

}
