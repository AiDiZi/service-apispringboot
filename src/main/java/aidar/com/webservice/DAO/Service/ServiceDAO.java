package aidar.com.webservice.DAO.Service;

import aidar.com.webservice.DAO.DBModelDAO;
import aidar.com.webservice.Models.Service;


public interface ServiceDAO extends DBModelDAO<Service> {

}
