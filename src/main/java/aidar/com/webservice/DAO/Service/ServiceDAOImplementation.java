package aidar.com.webservice.DAO.Service;

import aidar.com.webservice.DAO.ModifyAction.GenericModifyActions;
import aidar.com.webservice.DAO.RequestBuider;
import aidar.com.webservice.DAO.Service.CUDServiceActions.*;
import aidar.com.webservice.Models.Customer;
import aidar.com.webservice.Models.Service;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;

import java.util.AbstractMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public class ServiceDAOImplementation implements ServiceDAO {

    @Autowired
    private SessionFactory _sessionFactory;

    private GenericModifyActions _genericModifyActions;

    public ServiceDAOImplementation(){
        _genericModifyActions = new GenericModifyActions(_sessionFactory);
    }

    @Override
    public Optional<List<Service>> GetByParametrs(Set<AbstractMap.SimpleEntry<String, String>> parametrs ){
        var session = _sessionFactory.openSession();
        var parametrsStr = new RequestBuider().BuildParametrsForSelectRequest(parametrs);
        Optional<List<Service>> services = Optional.ofNullable
                (session.createQuery("from Service " + parametrsStr).list());
        session.close();
        return services;
    }

    @Override
    public Optional<Service> GetById(int id){
        var session = _sessionFactory.openSession();
        var service = session.get(Service.class, id);
        return Optional.ofNullable(service);
    }

    @Override
    public List<Service> GetAll(){
        var session = _sessionFactory.openSession();
        var services = session.createQuery("from Service").list();
        return services;
    }

    @Override
    public void Add(Service service){
        var addAction = new AddActionService();
        _genericModifyActions.ModifyAction(service, addAction);
    }

    @Override
    public void AddRange(List<Service> services){
        var addAction = new AddActionService();
        _genericModifyActions.ModifyActionRange(services, addAction);
    }

    @Override
    public void Update(Service service){
        var updateAction = new UpdateActionService();
        _genericModifyActions.ModifyAction(service, updateAction);
    }

    @Override
    public void UpdateRange(List<Service> services){
        var updateAction = new UpdateActionService();
        _genericModifyActions.ModifyActionRange(services, updateAction);
    }

    @Override
    public void Delete(Service service){
        var deleteAction = new DeleteActionService();
        _genericModifyActions.ModifyAction(service, deleteAction);
    }

    @Override
    public void DeleteRange(List<Service> services){
        var deleteAction = new DeleteActionService();
        _genericModifyActions.ModifyActionRange(services, deleteAction);
    }

}
