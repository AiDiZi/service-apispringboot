package aidar.com.webservice.DAO;

import aidar.com.webservice.Models.Customer;

import java.lang.reflect.Array;
import java.util.AbstractMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface DBModelDAO<T> {

    Optional<List<T>> GetByParametrs(Set<AbstractMap.SimpleEntry<String, String>> parametrs);

    Optional<T> GetById(int id);

    List<T> GetAll();

    void Add(T model);

    void AddRange(List<T> models);

    void Update(T model);

    void UpdateRange(List<T> models);

    void Delete(T model);

    void DeleteRange(List<T> models);

}
