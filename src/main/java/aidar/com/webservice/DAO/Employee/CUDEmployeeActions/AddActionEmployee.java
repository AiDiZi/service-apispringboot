package aidar.com.webservice.DAO.Employee.CUDEmployeeActions;

import aidar.com.webservice.DAO.ModifyAction.ModifyAction;
import aidar.com.webservice.Models.Employee;
import org.hibernate.Session;

public class AddActionEmployee implements ModifyAction<Employee> {

    @Override
    public void DoModifyAction(Employee employee, Session session){
        session.save(employee);
    }

}
