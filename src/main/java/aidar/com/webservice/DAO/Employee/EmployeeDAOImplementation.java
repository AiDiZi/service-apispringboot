package aidar.com.webservice.DAO.Employee;

import aidar.com.webservice.DAO.Customer.CustomerDAOImplementation;
import aidar.com.webservice.DAO.Employee.CUDEmployeeActions.*;
import aidar.com.webservice.DAO.ModifyAction.GenericModifyActions;
import aidar.com.webservice.DAO.RequestBuider;
import aidar.com.webservice.Models.Customer;
import aidar.com.webservice.Models.Employee;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;

import java.util.AbstractMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public class EmployeeDAOImplementation implements EmployeeDAO {

    private GenericModifyActions _genericModifyActions;

    @Autowired
    private SessionFactory _sessionFactory;

    public EmployeeDAOImplementation(){
        _genericModifyActions = new GenericModifyActions(_sessionFactory);
    }

    @Override
    public Optional<List<Employee>> GetByParametrs(Set<AbstractMap.SimpleEntry<String, String>> parametrs ){
        var session = _sessionFactory.openSession();
        var parametrsStr = new RequestBuider().BuildParametrsForSelectRequest(parametrs);
        Optional<List<Employee>> employees = Optional.ofNullable
                                                (session.createQuery("from Employee " + parametrsStr).list());
        session.close();
        return employees;
    }

    @Override
    public Optional<Employee> GetById(int id){
        var session = _sessionFactory.openSession();
        var employee = session.get(Employee.class, id);
        session.close();
        return Optional.ofNullable(employee);
    }

    @Override
    public List<Employee> GetAll(){
        var session =_sessionFactory.openSession();
        var employeeList = session.createQuery("from Employee").list();
        return employeeList;
    }

    @Override
    public void Add(Employee employee){
        var addAction = new AddActionEmployee();
        _genericModifyActions.ModifyAction(employee, addAction);
    }

    @Override
    public void AddRange(List<Employee> employeeList){
        var addAction = new AddActionEmployee();
        _genericModifyActions.ModifyActionRange(employeeList, addAction);
    }

    @Override
    public void Update(Employee employee){
        var updateAction = new UpdateActionEmployee();
        _genericModifyActions.ModifyAction(employee, updateAction);
    }

    @Override
    public void UpdateRange(List<Employee> employeeList){
        var updateAction = new UpdateActionEmployee();
        _genericModifyActions.ModifyActionRange(employeeList, updateAction);
    }

    public void Delete(Employee employee){
        var deleteAction = new DeleteActionEmployee();
        _genericModifyActions.ModifyAction(employee, deleteAction);
    }

    public void DeleteRange(List<Employee> employeeList){
        var deleteAction = new DeleteActionEmployee();
        _genericModifyActions.ModifyActionRange(employeeList, deleteAction);
    }
}
