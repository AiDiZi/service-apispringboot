package aidar.com.webservice.DAO.Employee.CUDEmployeeActions;

import aidar.com.webservice.DAO.ModifyAction.ModifyAction;
import aidar.com.webservice.Models.Employee;
import org.hibernate.Session;

public class UpdateActionEmployee implements ModifyAction<Employee> {

    @Override
    public void DoModifyAction(Employee employee, Session session){
        session.update(employee);
    }

}
