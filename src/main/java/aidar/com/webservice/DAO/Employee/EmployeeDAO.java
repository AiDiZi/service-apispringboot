package aidar.com.webservice.DAO.Employee;

import aidar.com.webservice.DAO.DBModelDAO;
import aidar.com.webservice.Models.Employee;

public interface EmployeeDAO extends DBModelDAO<Employee> {
}
