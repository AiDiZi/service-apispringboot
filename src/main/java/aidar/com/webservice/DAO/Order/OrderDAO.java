package aidar.com.webservice.DAO.Order;

import aidar.com.webservice.DAO.DBModelDAO;
import aidar.com.webservice.Models.Order;

public interface OrderDAO extends DBModelDAO<Order> {

}