package aidar.com.webservice.DAO.Order.CUDOrderActions;

import aidar.com.webservice.DAO.ModifyAction.ModifyAction;
import aidar.com.webservice.Models.Order;
import org.hibernate.Session;

public class AddActionOrder implements ModifyAction<Order> {

    @Override
    public void DoModifyAction(Order order, Session session){
        session.save(order);
    }

}
