package aidar.com.webservice.DAO.Order;

import aidar.com.webservice.DAO.ModifyAction.GenericModifyActions;
import aidar.com.webservice.DAO.ModifyAction.ModifyAction;
import aidar.com.webservice.DAO.Order.CUDOrderActions.*;
import aidar.com.webservice.DAO.RequestBuider;
import aidar.com.webservice.Models.Customer;
import aidar.com.webservice.Models.Order;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;

import java.util.AbstractMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public class OrderDAOImplementation implements OrderDAO {

    @Autowired
    private SessionFactory _sessionFactory;

    private GenericModifyActions<Order> _genericModifyActions;

    public OrderDAOImplementation(){
        _genericModifyActions = new GenericModifyActions<Order>(_sessionFactory);
    }

    @Override
    public Optional<List<Order>> GetByParametrs(Set<AbstractMap.SimpleEntry<String, String>> parametrs ){
        var session = _sessionFactory.openSession();
        var parametrsStr = new RequestBuider().BuildParametrsForSelectRequest(parametrs);
        Optional<List<Order>> orders = Optional.ofNullable
                                        (session.createQuery("from Order " + parametrsStr).list());
        session.close();
        return orders;
    }

    @Override
    public Optional<Order> GetById(int id){
        var session = _sessionFactory.openSession();
        var order = session.get(Order.class, id);
        session.close();
        return Optional.ofNullable(order);
    }

    @Override
    public List<Order> GetAll(){
        var session = _sessionFactory.openSession();
        var orders = session.createQuery("from Order").list();
        return orders;
    }

    @Override
    public void Add(Order order){
        var addAction = new AddActionOrder();
        _genericModifyActions.ModifyAction(order, addAction);
    }

    @Override
    public void AddRange(List<Order> orders){
        var addAction = new AddActionOrder();
        _genericModifyActions.ModifyActionRange(orders, addAction);
    }

    @Override
    public void Update(Order order){
        var updateAction = new UpdateActionOrder();
        _genericModifyActions.ModifyAction(order, updateAction);
    }

    @Override
    public void UpdateRange(List<Order> orders){
        ModifyAction<Order> updateAction = (Order order, Session session) -> {session.update(order);};
        _genericModifyActions.ModifyActionRange(orders, updateAction);
    }

    @Override
    public void Delete(Order order){
        var deleteAction = new DeleteActionOrder();
        _genericModifyActions.ModifyAction(order, deleteAction);
    }

    @Override
    public void DeleteRange(List<Order> orders){
        ModifyAction<Order> deleteAction = (Order order, Session session) -> {session.delete(order);};
        _genericModifyActions.ModifyActionRange(orders, deleteAction);
    }

}
