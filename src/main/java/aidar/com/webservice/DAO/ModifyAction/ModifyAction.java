package aidar.com.webservice.DAO.ModifyAction;

import org.hibernate.Session;

@FunctionalInterface
public interface ModifyAction<T> {
    void DoModifyAction(T model, Session session);
}
