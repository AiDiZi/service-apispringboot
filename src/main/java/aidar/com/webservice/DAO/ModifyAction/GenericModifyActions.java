package aidar.com.webservice.DAO.ModifyAction;

import org.hibernate.*;
import java.util.List;

public class GenericModifyActions<T>{

    private SessionFactory _sessionFactory;

    public GenericModifyActions(SessionFactory sessionFactory){
        _sessionFactory = sessionFactory;
    }

    public void ModifyAction(T model, ModifyAction action){
        var session = _sessionFactory.openSession();
        var transaction = session.beginTransaction();
        try{
            action.DoModifyAction(model, session);
            transaction.commit();
        }
        catch (Exception exception){
            transaction.rollback();
            System.out.println(exception.getStackTrace());
        }
        finally {
            session.close();
        }
    }

    public void ModifyActionRange(List<T> models, ModifyAction action){
        var session = _sessionFactory.openSession();
        var transaction = session.beginTransaction();
        try{
            for(var model : models)
                action.DoModifyAction(model, session);
            transaction.commit();
        }
        catch (Exception exception){
            transaction.rollback();
            System.out.println(exception.getStackTrace());
        }
        finally {
            session.close();
        }
    }
}
