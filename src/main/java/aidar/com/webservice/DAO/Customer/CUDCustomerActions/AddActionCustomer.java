package aidar.com.webservice.DAO.Customer.CUDCustomerActions;

import aidar.com.webservice.DAO.ModifyAction.ModifyAction;
import aidar.com.webservice.Models.Customer;
import org.hibernate.Session;

public class AddActionCustomer implements ModifyAction<Customer> {

    @Override
    public void DoModifyAction(Customer customer, Session session){
        session.save(customer);
    }

}
