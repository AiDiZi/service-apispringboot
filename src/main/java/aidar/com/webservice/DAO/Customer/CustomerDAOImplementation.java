package aidar.com.webservice.DAO.Customer;

import aidar.com.webservice.DAO.Customer.CUDCustomerActions.*;
import aidar.com.webservice.DAO.ModifyAction.GenericModifyActions;
import aidar.com.webservice.DAO.ModifyAction.ModifyAction;
import aidar.com.webservice.DAO.RequestBuider;
import aidar.com.webservice.Models.Customer;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.*;

@Repository
public class CustomerDAOImplementation implements CustomerDAO{

    private GenericModifyActions _genericModifyActions;

    @Autowired
    private SessionFactory _sessionFactory;

    public CustomerDAOImplementation(){
        _genericModifyActions = new GenericModifyActions<Customer>(_sessionFactory);
    }

    @Override
    public Optional<List<Customer>> GetByParametrs(Set<AbstractMap.SimpleEntry<String, String>> parametrs ){
        var session = _sessionFactory.openSession();
        var parametrsStr = new RequestBuider().BuildParametrsForSelectRequest(parametrs);
        Optional<List<Customer>> customers = Optional.ofNullable
                                            (session.createQuery("from Customer" + parametrsStr).list());
        session.close();
        return customers;
    }

    @Override
    public Optional<Customer> GetById(int id){
        var session = _sessionFactory.openSession();
        var customer = session.get(Customer.class, id);
        session.close();
        return Optional.ofNullable(customer);
    }

    @Override
    public List<Customer> GetAll(){
        var session = _sessionFactory.openSession();
        var customers = session.createQuery("FROM Customer").list();
        return customers;
    }

    @Override
    public void Add(Customer customer) {
        ModifyAction addAction = new AddActionCustomer();
        _genericModifyActions.ModifyAction(customer, addAction);
    }

    @Override
    public void AddRange(List<Customer> customers){
        ModifyAction addAction = new AddActionCustomer();
        _genericModifyActions.ModifyActionRange(customers, addAction);
    }

    @Override
    public void Update(Customer customer){
        ModifyAction<Customer> updateAction = new UpdateActionCustomer();
        _genericModifyActions.ModifyAction(customer, updateAction);
    }

    @Override
    public void UpdateRange(List<Customer> customers){
        ModifyAction<Customer> updateAction = new UpdateActionCustomer();
        _genericModifyActions.ModifyActionRange(customers, updateAction );

    }

    @Override
    public void Delete(Customer customer){
        ModifyAction<Customer> deleteAction = new DeleteActionCustomer();
        _genericModifyActions.ModifyAction(customer, deleteAction);
    }

    @Override
    public void DeleteRange(List<Customer> customers){
        ModifyAction<Customer> deleteAction = new DeleteActionCustomer();
        _genericModifyActions.ModifyActionRange(customers, deleteAction);
    }
}
