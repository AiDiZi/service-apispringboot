package aidar.com.webservice.DAO.Customer;


import aidar.com.webservice.DAO.DBModelDAO;
import aidar.com.webservice.Models.Customer;

public interface CustomerDAO extends DBModelDAO<Customer> {

}
