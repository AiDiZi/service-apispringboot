package aidar.com.webservice.DAO.Person;

import aidar.com.webservice.DAO.DBModelDAO;
import aidar.com.webservice.Models.Person;

public interface PersonDAO extends DBModelDAO<Person> {

}
