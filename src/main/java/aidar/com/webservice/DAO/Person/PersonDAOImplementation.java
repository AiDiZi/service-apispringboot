package aidar.com.webservice.DAO.Person;

import aidar.com.webservice.DAO.ModifyAction.GenericModifyActions;
import aidar.com.webservice.DAO.Person.CUDPersonActions.*;
import aidar.com.webservice.DAO.RequestBuider;
import aidar.com.webservice.Models.Customer;
import aidar.com.webservice.Models.Person;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;

import java.util.AbstractMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public class PersonDAOImplementation implements PersonDAO{

    @Autowired
    private SessionFactory _sessionFactory;

    private GenericModifyActions<Person> _genericModifyActions;

    public PersonDAOImplementation(){
        _genericModifyActions = new GenericModifyActions<Person>(_sessionFactory);
    }

    @Override
    public Optional<List<Person>> GetByParametrs(Set<AbstractMap.SimpleEntry<String, String>> parametrs ){
        var session = _sessionFactory.openSession();
        var parametrsStr = new RequestBuider().BuildParametrsForSelectRequest(parametrs);
        Optional<List<Person>> persons = Optional.ofNullable
                (session.createQuery("from Person " + parametrsStr).list());
        session.close();
        return persons;
    }

    @Override
    public Optional<Person> GetById(int id){
        var session = _sessionFactory.openSession();
        var person = session.get(Person.class, id);
        session.close();
        return Optional.ofNullable(person);
    }

    @Override
    public List<Person> GetAll(){
        var session = _sessionFactory.openSession();
        var persons = session.createQuery("from Person").list();
        return persons;
    }

    @Override
    public void Add(Person person){
        var addAction = new AddActionPerson();
        _genericModifyActions.ModifyAction(person, addAction);
    }

    public void AddRange(List<Person> peesons){
        var addAction= new AddActionPerson();
        _genericModifyActions.ModifyActionRange(peesons, addAction);
    }

    @Override
    public void Update(Person person){
        var updateAction = new UpdateActionPerson();
        _genericModifyActions.ModifyAction(person, updateAction);
    }

    @Override
    public void UpdateRange(List<Person> persons){
        var updateAction = new UpdateActionPerson();
        _genericModifyActions.ModifyActionRange(persons, updateAction);
    }

    @Override
    public void Delete(Person person){
        var deleteAction = new DeleteActionPerson();
        _genericModifyActions.ModifyAction(person, deleteAction);
    }

    @Override
    public void DeleteRange(List<Person> persons){
        var deleteAction = new DeleteActionPerson();
        _genericModifyActions.ModifyActionRange(persons, deleteAction);
    }

}
