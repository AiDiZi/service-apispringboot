package aidar.com.webservice.DAO.Person.CUDPersonActions;

import aidar.com.webservice.DAO.ModifyAction.ModifyAction;
import aidar.com.webservice.Models.Person;
import org.hibernate.Session;

public class AddActionPerson implements ModifyAction<Person> {

    @Override
    public void DoModifyAction(Person person, Session session){
        session.save(person);
    }

}
