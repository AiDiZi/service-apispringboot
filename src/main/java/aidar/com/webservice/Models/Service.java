package aidar.com.webservice.Models;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "Service")
public class Service
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "price")
    private float price;

    @Column(name = "nameService")
    private String nameService;

    @ManyToMany
    @JoinTable(name = "orderService",
               joinColumns = @JoinColumn(name = "serviceId"),
               inverseJoinColumns = @JoinColumn(name = "orderId"))
    private List<Order> orders;

    //get,set
    //id
    public int getId(){
        return id;
    }

    //price
    public void setPrice(float price){
        this.price = price;
    }

    public float getPrice(){
        return price;
    }

    //nameService
    public void setNameService(String nameService){
        this.nameService = nameService;
    }

    public String getNameService(){
        return nameService;
    }

    //orders
    public void setOrders(List<Order> orders){
        this.orders = orders;
    }

    public List<Order> getOrders(){
        return orders;
    }
}
