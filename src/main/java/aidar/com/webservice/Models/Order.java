package aidar.com.webservice.Models;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "Order")
public class Order {

    public Order(){}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "orderId")
    private Customer customer;

    @ManyToOne(fetch = FetchType.LAZY)

    private Employee employee;

    @ManyToMany
    @JoinTable(name = "orderService",
               joinColumns = @JoinColumn(name = "orderId"),
               inverseJoinColumns = @JoinColumn(name = "serviceId"))
    private List<Service> services;

    //get,set
    //id
    public int getId(){
        return id;
    }

    //customer
    public void setCustomers(Customer customer){
        this.customer = customer;
    }

    public Customer GetCustomer(){
        return  customer;
    }

    //employee
    public void setEmployee(Employee employee){
        this.employee = employee;
    }

    public Employee getEmployee(){
        return employee;
    }

    //services
    public void setServices(List<Service> services){
        this.services = services;
    }

    public List<Service> getServices(){
        return services;
    }
}
