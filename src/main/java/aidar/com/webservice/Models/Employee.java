package aidar.com.webservice.Models;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Employee")
@PrimaryKeyJoinColumn(name = "id")
public class Employee extends Person
{
    public Employee(){}

    @Column(name = "salary")
    private float salary;

    @Column(name = "birthday")
    private Date birthDate;

    @Column(name = "phoneNumber")
    private String phoneNumber;

    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    private List<Order> orders;

    //get,set
    //salary
    public void setSalary(float salary){
        this.salary = salary;
    }

    public float getSalary(){
        return salary;
    }

    //birthdate
    public void setBirthDate(Date birthDate){
        this.birthDate = birthDate;
    }

    public Date getBirthDate(){
        return birthDate;
    }

    //phoneNumber
    public void setPhoneNumber(String phoneNumber){
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber(){
        return phoneNumber;
    }

    //orders
    public void setOrders(List<Order> orders){
        this.orders = orders;
    }

    public List<Order> getOrders(){
        return orders;
    }

}
