package aidar.com.webservice.Models;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "Person")
@Inheritance(strategy=InheritanceType.JOINED)
public class Person
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")

    private String lastName;

    @Column(name = "phoneNumber")
    private String phoneNumber;

    //get,set
    //id
    public int getId(){
        return id;
    }
    //firstName
    public void setFirstName(String firstName){
        this.firstName = firstName;
    }

    public String getFirstName(){
        return  firstName;
    }

    //lastName
    public void setLastName(String lastName){
        this.lastName = lastName;
    }

    public String getLastName(){
        return  lastName;
    }

    //phoneNumber
    public void setPhoneNumber(String phoneNumber){
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber(){
        return phoneNumber;
    }
}
