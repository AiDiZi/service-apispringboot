package aidar.com.webservice.Models;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "Customer")
@PrimaryKeyJoinColumn(name="id")
public class Customer extends Person
{


    public Customer(){}

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Order> orders;

    //get,set
    //orders
    public void setOrders(List<Order> orders){
        this.orders = orders;
    }

    public List<Order> getOrders(){
        return orders;
    }

}
