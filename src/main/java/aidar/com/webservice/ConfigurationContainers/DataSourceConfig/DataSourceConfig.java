package aidar.com.webservice.ConfigurationContainers.DataSourceConfig;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration

public class DataSourceConfig {

    /*@Autowired
    private Environment _env;

    @Bean(name = "dataSource")
    public DataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        // See: application.properties
        dataSource.setDriverClassName(_env.getProperty("spring.datasource.driver-class-name"));
        dataSource.setUrl(_env.getProperty("spring.datasource.url"));
        dataSource.setUsername(_env.getProperty("spring.datasource.username"));
        dataSource.setPassword(_env.getProperty("spring.datasource.password"));

        System.out.println("## getDataSource: " + dataSource);

        return dataSource;
    }

    @Bean(name = "sessionFactory")
    public SessionFactory getSessionFactory(DataSource dataSource) throws Exception {
        Properties properties = new Properties();

        // See: application.properties
        properties.put("hibernate.dialect", _env.getProperty("spring.jpa.properties.hibernate.dialect"));
        properties.put("hibernate.show_sql", _env.getProperty("spring.jpa.show-sql"));
        properties.put("current_session_context_class", //
                _env.getProperty("spring.jpa.properties.hibernate.current_session_context_class"));


        LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();

        // Package contain entity classes
        factoryBean.setPackagesToScan(new String[] { "" });
        factoryBean.setDataSource(dataSource);
        factoryBean.setHibernateProperties(properties);
        //
        SessionFactory sf = factoryBean.getObject();
        System.out.println("## getSessionFactory: " + sf);
        return sf;
    }*/
}
