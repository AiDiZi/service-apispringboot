package aidar.com.webservice.ConfigurationContainers.DAOContainer;

import aidar.com.webservice.DAO.Customer.CustomerDAO;
import aidar.com.webservice.DAO.Customer.CustomerDAOImplementation;
import aidar.com.webservice.DAO.Employee.EmployeeDAOImplementation;
import aidar.com.webservice.DAO.Order.OrderDAO;
import aidar.com.webservice.DAO.Order.OrderDAOImplementation;
import aidar.com.webservice.DAO.Person.PersonDAOImplementation;
import aidar.com.webservice.DAO.Service.ServiceDAOImplementation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DAODBBeanContainer {

    @Bean(name = "CustomerDAODB")
    public CustomerDAO GetCustomerDAO(){
        return new CustomerDAOImplementation();
    }

    @Bean(name = "EmployeeDAODB")
    public EmployeeDAOImplementation GetEmployeeDAO(){
        return new EmployeeDAOImplementation();
    }

    @Bean(name = "OrderDAODB")
    public OrderDAO GetOrderDAOImpelementation(){
        return  new OrderDAOImplementation();
    }

    @Bean(name = "PersonDAODB")
    public PersonDAOImplementation GetPersonDAOImplementation(){
        return  new PersonDAOImplementation();
    }

    @Bean(name = "ServiceDAODB")
    public ServiceDAOImplementation GetServiceDAOImplementation(){
        return  new ServiceDAOImplementation();
    }

}
