package aidar.com.webservice.ConfigurationContainers.ServicesContainer;

import aidar.com.webservice.Services.CustomerServices.CustomerServices;
import aidar.com.webservice.Services.CustomerServices.ICustomerServices;
import aidar.com.webservice.Services.EmployeeServices.EmployeeService;
import aidar.com.webservice.Services.EmployeeServices.IEmployeeServices;
import aidar.com.webservice.Services.OrderService.IOrderService;
import aidar.com.webservice.Services.OrderService.OrderService;
import aidar.com.webservice.Services.PersonServices.IPersonService;
import aidar.com.webservice.Services.PersonServices.PersonService;
import aidar.com.webservice.Services.ServiceServices.IServiceServices;
import aidar.com.webservice.Services.ServiceServices.ServiceServices;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceDBBeanContainer {

    @Bean(name = "CustomerServiceDB")
    public ICustomerServices GetCustomerService(){
        return new CustomerServices();
    }

    @Bean(name = "EmployeeServiceDB")
    public IEmployeeServices GetEmployeeService(){
        return new EmployeeService();
    }

    @Bean(name = "OrderServiceDB")
    public IOrderService GetOrderSevice(){
        return new OrderService();
    }

    @Bean(name = "PersonServiceDB")
    public IPersonService GetPersonService(){
        return new PersonService();
    }

    @Bean(name = "ServiceServiceDB")
    public IServiceServices GetServiceService(){
        return  new ServiceServices();
    }
}
